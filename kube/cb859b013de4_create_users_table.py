"""create users table

Revision ID: cb859b013de4
Revises: 
Create Date: 2019-03-25 23:27:04.280466

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID


# revision identifiers, used by Alembic.
revision = 'cb859b013de4'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'users',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('uid', UUID()),
        sa.Column('name', sa.String(255), nullable=False),
        sa.Column('email', sa.String(255)),
        sa.Column('password', sa.String(255)),
        sa.Column('created_at', sa.TIMESTAMP, server_default=sa.func.now()),
        # sa.Column('enabled', sa.Boolean(), nullable=False, server_default='true')
    )
    op.create_unique_constraint("uq_users_email", "users", ["email"])
    op.create_unique_constraint("uq_users_uid", "users", ["uid"])


def downgrade():
    op.drop_table('users')
