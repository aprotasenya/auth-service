module auth-gateway

require (
	cloud.google.com/go v0.37.1 // indirect
	github.com/GoogleCloudPlatform/cloudsql-proxy v0.0.0-20190312192040-a2a65ffce834 // indirect
	github.com/Shopify/sarama v1.21.0 // indirect
	github.com/aclements/go-gg v0.0.0-20170323211221-abd1f791f5ee // indirect
	github.com/aclements/go-moremath v0.0.0-20180329182055-b1aff36309c7 // indirect
	github.com/coreos/go-systemd v0.0.0-20190321100706-95778dfbb74e // indirect
	github.com/gliderlabs/ssh v0.1.3 // indirect
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/golang/protobuf v1.3.1
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/btree v1.0.0 // indirect
	github.com/google/pprof v0.0.0-20190309163659-77426154d546 // indirect
	github.com/googleapis/gax-go v2.0.2+incompatible // indirect
	github.com/gorilla/mux v1.7.0 // indirect
	github.com/gregjones/httpcache v0.0.0-20190212212710-3befbb6ad0cc // indirect
	github.com/grpc-ecosystem/grpc-gateway v1.8.5
	github.com/hashicorp/golang-lru v0.5.1 // indirect
	github.com/kisielk/errcheck v1.2.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/kr/pty v1.1.4 // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/prometheus/client_model v0.0.0-20190129233127-fd36f4220a90 // indirect
	github.com/prometheus/procfs v0.0.0-20190322151404-55ae3d9d5573 // indirect
	github.com/rogpeppe/fastuuid v1.0.0 // indirect
	github.com/sirupsen/logrus v1.4.0 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	go.opencensus.io v0.19.2 // indirect
	go4.org v0.0.0-20190313082347-94abd6928b1d // indirect
	golang.org/x/build v0.0.0-20190322222834-9f5f576c2adc // indirect
	golang.org/x/crypto v0.0.0-20190320223903-b7391e95e576 // indirect
	golang.org/x/exp v0.0.0-20190321205749-f0864edee7f3 // indirect
	golang.org/x/image v0.0.0-20190321063152-3fc05d484e9f // indirect
	golang.org/x/lint v0.0.0-20190313153728-d0100b6bd8b3 // indirect
	golang.org/x/mobile v0.0.0-20190319155245-9487ef54b94a // indirect
	golang.org/x/net v0.0.0-20190324223953-e3b2ff56ed87
	golang.org/x/oauth2 v0.0.0-20190319182350-c85d3e98c914 // indirect
	golang.org/x/perf v0.0.0-20190312170614-0655857e383f // indirect
	golang.org/x/sys v0.0.0-20190322080309-f49334f85ddc // indirect
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4 // indirect
	golang.org/x/tools v0.0.0-20190322203728-c1a832b0ad89 // indirect
	google.golang.org/appengine v1.5.0 // indirect
	google.golang.org/genproto v0.0.0-20190321212433-e79c0c59cdb5
	google.golang.org/grpc v1.19.1
	gopkg.in/yaml.v2 v2.2.2 // indirect
	honnef.co/go/tools v0.0.0-20190319011948-d116c56a00f3 // indirect
)

replace github.com/golang/lint => github.com/golang/lint v0.0.0-20190227174305-8f45f776aaf1
