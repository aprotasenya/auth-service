package main

import (
	"flag"
	"net/http"

	"github.com/golang/glog"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"golang.org/x/net/context"
	"google.golang.org/grpc"

	gw "auth-gateway/auth"
)

var (
	echoEndpoint = flag.String("echo_endpoint", "auth-service.local:80", "endpoint of YourService")
)

func run() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := http.NewServeMux()
	mux.HandleFunc("/swagger.json", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "/swagger.json")
	})

	mux.Handle(
		"/swagger/",
		http.StripPrefix(
			"/swagger/",
			http.FileServer(http.Dir("/swagger-ui"))))

	gwMux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}

	err := gw.RegisterAuthServiceHandlerFromEndpoint(ctx, gwMux, *echoEndpoint, opts)
	if err != nil {
		return err
	}

	err = gw.RegisterUsersServiceHandlerFromEndpoint(ctx, gwMux, *echoEndpoint, opts)
	if err != nil {
		return err
	}

	mux.Handle("/", gwMux)

	return http.ListenAndServe(":80", mux)
}

func main() {
	flag.Parse()
	defer glog.Flush()

	if err := run(); err != nil {
		glog.Fatal(err)
	}
}
