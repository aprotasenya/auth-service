import sqlalchemy as sa
from sqlalchemy import (TIMESTAMP, Column, Integer,
                        String)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.inspection import inspect as _inspect

from sqlalchemy_continuum import make_versioned


make_versioned(user_cls=None)


class BaseCls(object):

    def to_dict(self):
        column_names = _inspect(self.__class__).columns.keys()
        return {k: self.__dict__[k] for k in column_names}


BaseModel = declarative_base(cls=BaseCls)


class UserModel(BaseModel):
    __versioned__ = {}
    __tablename__ = 'users'
    id = Column('id', Integer(), primary_key=True)
    created_at = Column('created_at', TIMESTAMP, default=sa.func.now())
    name = Column('name', String(255), nullable=False)
    email = Column('email', String(255))
    password = Column('password', String(60))

    __unique__ = ['uid, email']

    def __repr__(self):
        return '<Item(uid=%s, name=%s, email=%s)>' % (self.id, self.name, self.email)


sa.orm.configure_mappers()
