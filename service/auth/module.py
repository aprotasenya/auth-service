import os

from injector import Binder, Module, singleton
from sqlalchemy_wrapper import SQLAlchemy

import auth.services as services


class IConfiguration(Module):
    def __init__(self):
        pass

    def configure(self, binder: Binder):
        binder.bind(SQLAlchemy, to=SQLAlchemy(os.environ['DATABASE_URL']), scope=singleton)

        for classes in (services,):
            for class_name in classes.__all__:
                clc = getattr(classes, class_name)
                binder.bind(clc, to=clc, scope=singleton)
