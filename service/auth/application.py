import time
from concurrent import futures

import grpc
from injector import inject

from auth import auth_pb2_grpc
from auth.services import AuthService, UsersService

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class Application():
    @inject
    def __init__(self, users: UsersService, auth: AuthService):
        self.users = users
        self.auth = auth

    def run(self):
        server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
        auth_pb2_grpc.add_UsersServiceServicer_to_server(self.users, server)
        auth_pb2_grpc.add_AuthServiceServicer_to_server(self.auth, server)
        server.add_insecure_port('[::]:80')
        server.start()
        try:
            while True:
                time.sleep(_ONE_DAY_IN_SECONDS)
        except KeyboardInterrupt:
            server.stop(0)
