from injector import Injector

from auth.application import Application
from auth.module import IConfiguration


def get_application():
    injector = Injector(modules=[IConfiguration])
    return injector.get(Application)
