import datetime
import logging
import sys

import bcrypt
import jwt
from injector import inject
from sqlalchemy_wrapper import SQLAlchemy

from auth import auth_pb2, auth_pb2_grpc
from auth.models import UserModel

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
log.addHandler(ch)


class AuthService(auth_pb2_grpc.AuthServiceServicer):
    @inject
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def CreateToken(self, request, context):
        error_response = auth_pb2.Response(errors=[auth_pb2.Error(code=404, description='User not found')])
        try:
            user = self.db.query(UserModel).filter(UserModel.email == request.email).one()

            password_check = bcrypt.checkpw(request.password.encode(), user.password.encode())

            if not password_check:
                return error_response

            encoded_jwt = jwt.encode(
                {
                    'id': user.id,
                    'name': user.name,
                    'email': user.email,
                    'exp': datetime.datetime.utcnow() + datetime.timedelta(weeks=1)
                },
                'secret',
                algorithm='HS256')
            return auth_pb2.Token(token=encoded_jwt)
        except KeyboardInterrupt:
            return error_response

    def ValidateToken(self, request, context):
        try:
            valid = not not jwt.decode(request.token, 'secret', algorithms='HS256')
            return auth_pb2.Token(token=request.token, valid=valid)
        except:
            return auth_pb2.Token(token=request.token, valid=False)
