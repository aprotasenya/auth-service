import logging
import sys

import bcrypt
import jwt
from injector import inject
from sqlalchemy.exc import IntegrityError
from sqlalchemy_wrapper import SQLAlchemy

from auth import auth_pb2, auth_pb2_grpc
from auth.models import UserModel

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
log.addHandler(ch)


class UsersService(auth_pb2_grpc.UsersServiceServicer):
    @inject
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def Create(self, request, context):
        try:
            user = UserModel(
                name=request.name,
                email=request.email,
                password=bcrypt.hashpw(request.password.encode(), bcrypt.gensalt()).decode()
            )
            self.db.add(user)
            self.db.commit()
            log.info(f'created user with id {user.id}')
            return auth_pb2.Response(user=auth_pb2.User(id=user.id))
        except IntegrityError as err:
            self.db.rollback()
            return auth_pb2.Response(errors=[auth_pb2.Error(code=409, description=str(err))])

    def Update(self, request, context):
        user = self.db.query(UserModel).filter(UserModel.id == request.id).one()
        if request.name and user.name != request.name:
            user.name = request.name

        if request.email and user.email != request.email:
            user.email = request.email

        if request.password:
            user.password = bcrypt.hashpw(request.password.encode(), bcrypt.gensalt()).decode()

        self.db.commit()
        log.info(f'updated user with id {user.id}')
        return auth_pb2.Response(
            user=auth_pb2.User(
                id=user.id,
                name=user.name,
                email=user.email
            ))

    def GetAll(self, request, context):
        users = self.db.query(UserModel)
        return auth_pb2.Response(users=[
            auth_pb2.User(
                id=user.id,
                name=user.name,
                email=user.email
            )
            for user in users
        ])

    def Get(self, request, context):
        log.info(f'Get({request.id})')
        user = self.db.query(UserModel).filter(UserModel.id == request.id).one()
        return auth_pb2.Response(
            user=auth_pb2.User(
                id=user.id,
                name=user.name,
                email=user.email
            ))

    def Delete(self, request, context):
        user = self.db.query(UserModel).filter(UserModel.id == request.id).one()
        self.db.delete(user)
        self.db.commit()
        return auth_pb2.Response(user=auth_pb2.User(id=0))

    def Me(self, request, context):
        payload = jwt.decode(request.token, 'secret', algorithms='HS256')
        user = self.db.query(UserModel).filter(UserModel.id == payload.get('id')).one()
        return auth_pb2.Response(
            user=auth_pb2.User(
                id=user.id,
                name=user.name,
                email=user.email
            ))
