"""Add users table

Revision ID: ff7d2ba2c92b
Revises: 
Create Date: 2019-04-18 12:07:43.369944

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ff7d2ba2c92b'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('transaction',
    sa.Column('issued_at', sa.DateTime(), nullable=True),
    sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
    sa.Column('remote_addr', sa.String(length=50), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('users',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_at', sa.TIMESTAMP(), nullable=True),
    sa.Column('name', sa.String(length=255), nullable=False),
    sa.Column('email', sa.String(length=255), nullable=True),
    sa.Column('password', sa.String(length=60), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_unique_constraint("uq_users_email", "users", ["email"])
    op.create_table('users_version',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('created_at', sa.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.Column('name', sa.String(length=255), autoincrement=False, nullable=True),
    sa.Column('email', sa.String(length=255), autoincrement=False, nullable=True),
    sa.Column('password', sa.String(length=60), autoincrement=False, nullable=True),
    sa.Column('transaction_id', sa.BigInteger(), autoincrement=False, nullable=False),
    sa.Column('end_transaction_id', sa.BigInteger(), nullable=True),
    sa.Column('operation_type', sa.SmallInteger(), nullable=False),
    sa.PrimaryKeyConstraint('id', 'transaction_id')
    )
    op.create_index(op.f('ix_users_version_end_transaction_id'), 'users_version', ['end_transaction_id'], unique=False)
    op.create_index(op.f('ix_users_version_operation_type'), 'users_version', ['operation_type'], unique=False)
    op.create_index(op.f('ix_users_version_transaction_id'), 'users_version', ['transaction_id'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_users_version_transaction_id'), table_name='users_version')
    op.drop_index(op.f('ix_users_version_operation_type'), table_name='users_version')
    op.drop_index(op.f('ix_users_version_end_transaction_id'), table_name='users_version')
    op.drop_constraint('uq_users_email', 'users')
    op.drop_table('users_version')
    op.drop_table('users')
    op.drop_table('transaction')
    # ### end Alembic commands ###
