// package: auth
// file: auth.proto

/* tslint:disable */

import * as grpc from "grpc";
import * as auth_pb from "./auth_pb";

interface IUsersServiceService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    create: IUsersServiceService_ICreate;
    update: IUsersServiceService_IUpdate;
    get: IUsersServiceService_IGet;
    getAll: IUsersServiceService_IGetAll;
    delete: IUsersServiceService_IDelete;
    me: IUsersServiceService_IMe;
}

interface IUsersServiceService_ICreate extends grpc.MethodDefinition<auth_pb.User, auth_pb.Response> {
    path: string; // "/auth.UsersService/Create"
    requestStream: boolean; // false
    responseStream: boolean; // false
    requestSerialize: grpc.serialize<auth_pb.User>;
    requestDeserialize: grpc.deserialize<auth_pb.User>;
    responseSerialize: grpc.serialize<auth_pb.Response>;
    responseDeserialize: grpc.deserialize<auth_pb.Response>;
}
interface IUsersServiceService_IUpdate extends grpc.MethodDefinition<auth_pb.User, auth_pb.Response> {
    path: string; // "/auth.UsersService/Update"
    requestStream: boolean; // false
    responseStream: boolean; // false
    requestSerialize: grpc.serialize<auth_pb.User>;
    requestDeserialize: grpc.deserialize<auth_pb.User>;
    responseSerialize: grpc.serialize<auth_pb.Response>;
    responseDeserialize: grpc.deserialize<auth_pb.Response>;
}
interface IUsersServiceService_IGet extends grpc.MethodDefinition<auth_pb.User, auth_pb.Response> {
    path: string; // "/auth.UsersService/Get"
    requestStream: boolean; // false
    responseStream: boolean; // false
    requestSerialize: grpc.serialize<auth_pb.User>;
    requestDeserialize: grpc.deserialize<auth_pb.User>;
    responseSerialize: grpc.serialize<auth_pb.Response>;
    responseDeserialize: grpc.deserialize<auth_pb.Response>;
}
interface IUsersServiceService_IGetAll extends grpc.MethodDefinition<auth_pb.Request, auth_pb.Response> {
    path: string; // "/auth.UsersService/GetAll"
    requestStream: boolean; // false
    responseStream: boolean; // false
    requestSerialize: grpc.serialize<auth_pb.Request>;
    requestDeserialize: grpc.deserialize<auth_pb.Request>;
    responseSerialize: grpc.serialize<auth_pb.Response>;
    responseDeserialize: grpc.deserialize<auth_pb.Response>;
}
interface IUsersServiceService_IDelete extends grpc.MethodDefinition<auth_pb.User, auth_pb.Response> {
    path: string; // "/auth.UsersService/Delete"
    requestStream: boolean; // false
    responseStream: boolean; // false
    requestSerialize: grpc.serialize<auth_pb.User>;
    requestDeserialize: grpc.deserialize<auth_pb.User>;
    responseSerialize: grpc.serialize<auth_pb.Response>;
    responseDeserialize: grpc.deserialize<auth_pb.Response>;
}
interface IUsersServiceService_IMe extends grpc.MethodDefinition<auth_pb.Token, auth_pb.Response> {
    path: string; // "/auth.UsersService/Me"
    requestStream: boolean; // false
    responseStream: boolean; // false
    requestSerialize: grpc.serialize<auth_pb.Token>;
    requestDeserialize: grpc.deserialize<auth_pb.Token>;
    responseSerialize: grpc.serialize<auth_pb.Response>;
    responseDeserialize: grpc.deserialize<auth_pb.Response>;
}

export const UsersServiceService: IUsersServiceService;

export interface IUsersServiceServer {
    create: grpc.handleUnaryCall<auth_pb.User, auth_pb.Response>;
    update: grpc.handleUnaryCall<auth_pb.User, auth_pb.Response>;
    get: grpc.handleUnaryCall<auth_pb.User, auth_pb.Response>;
    getAll: grpc.handleUnaryCall<auth_pb.Request, auth_pb.Response>;
    delete: grpc.handleUnaryCall<auth_pb.User, auth_pb.Response>;
    me: grpc.handleUnaryCall<auth_pb.Token, auth_pb.Response>;
}

export interface IUsersServiceClient {
    create(request: auth_pb.User, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    create(request: auth_pb.User, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    create(request: auth_pb.User, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    update(request: auth_pb.User, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    update(request: auth_pb.User, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    update(request: auth_pb.User, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    get(request: auth_pb.User, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    get(request: auth_pb.User, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    get(request: auth_pb.User, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    getAll(request: auth_pb.Request, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    getAll(request: auth_pb.Request, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    getAll(request: auth_pb.Request, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    delete(request: auth_pb.User, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    delete(request: auth_pb.User, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    delete(request: auth_pb.User, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    me(request: auth_pb.Token, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    me(request: auth_pb.Token, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    me(request: auth_pb.Token, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
}

export class UsersServiceClient extends grpc.Client implements IUsersServiceClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public create(request: auth_pb.User, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public create(request: auth_pb.User, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public create(request: auth_pb.User, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public update(request: auth_pb.User, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public update(request: auth_pb.User, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public update(request: auth_pb.User, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public get(request: auth_pb.User, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public get(request: auth_pb.User, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public get(request: auth_pb.User, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public getAll(request: auth_pb.Request, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public getAll(request: auth_pb.Request, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public getAll(request: auth_pb.Request, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public delete(request: auth_pb.User, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public delete(request: auth_pb.User, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public delete(request: auth_pb.User, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public me(request: auth_pb.Token, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public me(request: auth_pb.Token, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
    public me(request: auth_pb.Token, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Response) => void): grpc.ClientUnaryCall;
}

interface IAuthServiceService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    createToken: IAuthServiceService_ICreateToken;
    validateToken: IAuthServiceService_IValidateToken;
}

interface IAuthServiceService_ICreateToken extends grpc.MethodDefinition<auth_pb.User, auth_pb.Token> {
    path: string; // "/auth.AuthService/CreateToken"
    requestStream: boolean; // false
    responseStream: boolean; // false
    requestSerialize: grpc.serialize<auth_pb.User>;
    requestDeserialize: grpc.deserialize<auth_pb.User>;
    responseSerialize: grpc.serialize<auth_pb.Token>;
    responseDeserialize: grpc.deserialize<auth_pb.Token>;
}
interface IAuthServiceService_IValidateToken extends grpc.MethodDefinition<auth_pb.Token, auth_pb.Token> {
    path: string; // "/auth.AuthService/ValidateToken"
    requestStream: boolean; // false
    responseStream: boolean; // false
    requestSerialize: grpc.serialize<auth_pb.Token>;
    requestDeserialize: grpc.deserialize<auth_pb.Token>;
    responseSerialize: grpc.serialize<auth_pb.Token>;
    responseDeserialize: grpc.deserialize<auth_pb.Token>;
}

export const AuthServiceService: IAuthServiceService;

export interface IAuthServiceServer {
    createToken: grpc.handleUnaryCall<auth_pb.User, auth_pb.Token>;
    validateToken: grpc.handleUnaryCall<auth_pb.Token, auth_pb.Token>;
}

export interface IAuthServiceClient {
    createToken(request: auth_pb.User, callback: (error: grpc.ServiceError | null, response: auth_pb.Token) => void): grpc.ClientUnaryCall;
    createToken(request: auth_pb.User, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Token) => void): grpc.ClientUnaryCall;
    createToken(request: auth_pb.User, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Token) => void): grpc.ClientUnaryCall;
    validateToken(request: auth_pb.Token, callback: (error: grpc.ServiceError | null, response: auth_pb.Token) => void): grpc.ClientUnaryCall;
    validateToken(request: auth_pb.Token, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Token) => void): grpc.ClientUnaryCall;
    validateToken(request: auth_pb.Token, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Token) => void): grpc.ClientUnaryCall;
}

export class AuthServiceClient extends grpc.Client implements IAuthServiceClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public createToken(request: auth_pb.User, callback: (error: grpc.ServiceError | null, response: auth_pb.Token) => void): grpc.ClientUnaryCall;
    public createToken(request: auth_pb.User, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Token) => void): grpc.ClientUnaryCall;
    public createToken(request: auth_pb.User, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Token) => void): grpc.ClientUnaryCall;
    public validateToken(request: auth_pb.Token, callback: (error: grpc.ServiceError | null, response: auth_pb.Token) => void): grpc.ClientUnaryCall;
    public validateToken(request: auth_pb.Token, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: auth_pb.Token) => void): grpc.ClientUnaryCall;
    public validateToken(request: auth_pb.Token, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: auth_pb.Token) => void): grpc.ClientUnaryCall;
}
