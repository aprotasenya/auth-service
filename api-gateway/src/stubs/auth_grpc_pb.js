// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var auth_pb = require('./auth_pb.js');
var google_api_annotations_pb = require('./google/api/annotations_pb.js');

function serialize_auth_Request(arg) {
  if (!(arg instanceof auth_pb.Request)) {
    throw new Error('Expected argument of type auth.Request');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_auth_Request(buffer_arg) {
  return auth_pb.Request.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_auth_Response(arg) {
  if (!(arg instanceof auth_pb.Response)) {
    throw new Error('Expected argument of type auth.Response');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_auth_Response(buffer_arg) {
  return auth_pb.Response.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_auth_Token(arg) {
  if (!(arg instanceof auth_pb.Token)) {
    throw new Error('Expected argument of type auth.Token');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_auth_Token(buffer_arg) {
  return auth_pb.Token.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_auth_User(arg) {
  if (!(arg instanceof auth_pb.User)) {
    throw new Error('Expected argument of type auth.User');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_auth_User(buffer_arg) {
  return auth_pb.User.deserializeBinary(new Uint8Array(buffer_arg));
}


var UsersServiceService = exports.UsersServiceService = {
  create: {
    path: '/auth.UsersService/Create',
    requestStream: false,
    responseStream: false,
    requestType: auth_pb.User,
    responseType: auth_pb.Response,
    requestSerialize: serialize_auth_User,
    requestDeserialize: deserialize_auth_User,
    responseSerialize: serialize_auth_Response,
    responseDeserialize: deserialize_auth_Response,
  },
  update: {
    path: '/auth.UsersService/Update',
    requestStream: false,
    responseStream: false,
    requestType: auth_pb.User,
    responseType: auth_pb.Response,
    requestSerialize: serialize_auth_User,
    requestDeserialize: deserialize_auth_User,
    responseSerialize: serialize_auth_Response,
    responseDeserialize: deserialize_auth_Response,
  },
  get: {
    path: '/auth.UsersService/Get',
    requestStream: false,
    responseStream: false,
    requestType: auth_pb.User,
    responseType: auth_pb.Response,
    requestSerialize: serialize_auth_User,
    requestDeserialize: deserialize_auth_User,
    responseSerialize: serialize_auth_Response,
    responseDeserialize: deserialize_auth_Response,
  },
  getAll: {
    path: '/auth.UsersService/GetAll',
    requestStream: false,
    responseStream: false,
    requestType: auth_pb.Request,
    responseType: auth_pb.Response,
    requestSerialize: serialize_auth_Request,
    requestDeserialize: deserialize_auth_Request,
    responseSerialize: serialize_auth_Response,
    responseDeserialize: deserialize_auth_Response,
  },
  delete: {
    path: '/auth.UsersService/Delete',
    requestStream: false,
    responseStream: false,
    requestType: auth_pb.User,
    responseType: auth_pb.Response,
    requestSerialize: serialize_auth_User,
    requestDeserialize: deserialize_auth_User,
    responseSerialize: serialize_auth_Response,
    responseDeserialize: deserialize_auth_Response,
  },
  me: {
    path: '/auth.UsersService/Me',
    requestStream: false,
    responseStream: false,
    requestType: auth_pb.Token,
    responseType: auth_pb.Response,
    requestSerialize: serialize_auth_Token,
    requestDeserialize: deserialize_auth_Token,
    responseSerialize: serialize_auth_Response,
    responseDeserialize: deserialize_auth_Response,
  },
};

exports.UsersServiceClient = grpc.makeGenericClientConstructor(UsersServiceService);
var AuthServiceService = exports.AuthServiceService = {
  createToken: {
    path: '/auth.AuthService/CreateToken',
    requestStream: false,
    responseStream: false,
    requestType: auth_pb.User,
    responseType: auth_pb.Token,
    requestSerialize: serialize_auth_User,
    requestDeserialize: deserialize_auth_User,
    responseSerialize: serialize_auth_Token,
    responseDeserialize: deserialize_auth_Token,
  },
  validateToken: {
    path: '/auth.AuthService/ValidateToken',
    requestStream: false,
    responseStream: false,
    requestType: auth_pb.Token,
    responseType: auth_pb.Token,
    requestSerialize: serialize_auth_Token,
    requestDeserialize: deserialize_auth_Token,
    responseSerialize: serialize_auth_Token,
    responseDeserialize: deserialize_auth_Token,
  },
};

exports.AuthServiceClient = grpc.makeGenericClientConstructor(AuthServiceService);
