import { AuthenticationError } from "apollo-server";

export const authenticated = (next: (root: any, args: any, context: any, info: any) => any) => (root: any, args: any, context: any, info: any) => {
    if (!context.auth) throw new AuthenticationError('must authenticate');
    return next(root, args, context, info);
}