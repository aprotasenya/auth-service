import { DataSource } from "apollo-datasource";
import * as grpc from "grpc";
import { UsersServiceClient } from '../stubs/auth_grpc_pb';
import { User, Request, Response, Token, Error } from "../stubs/auth_pb";


export class UsersDataSource extends DataSource {
    client: UsersServiceClient

    constructor() {
        super();
        this.client = new UsersServiceClient('auth-service:80', grpc.credentials.createInsecure());
    }

    async createUser(email: string, name: string, password: string) {
        return new Promise((resolve, reject) => {
            const request = new User();
            request.setEmail(email);
            request.setName(name);
            request.setPassword(password);
    
            this.client.create(request, (err, response: Response) => {
                if (err != null) {
                    reject(err); return;
                }

                const errors = <Array<Error>>response.getErrorsList();
                if (errors.length > 0) reject(errors.map((error) => error.toObject().code))

                const user = <User>response.getUser()
                if (user) resolve(user.toObject());
                else reject('user not created')
            });
        });
    }

    async updateUser(id: number, email?: string, name?: string, password?: string) {
        return new Promise((resolve, reject) => {
            const request = new User();
            request.setId(id);
            if (email) request.setEmail(email);
            if (name) request.setName(name);
            if (password) request.setPassword(password);
    
            this.client.update(request, (err, response: Response) => {
                if (err != null) {
                    reject(err); return;
                }
                const user = (<User>response.getUser()).toObject()
                resolve(user);
            });
        });
    }

    async deleteUser(id: number) {
        return new Promise((resolve, reject) => {
            const request = new User();
            request.setId(id);
    
            this.client.delete(request, (err, response: Response) => {
                if (err != null) {
                    reject(err); return;
                }
                const user = (<User>response.getUser()).toObject();
                resolve(user);
            });
        });
    }

    async getUsers() {
        return new Promise((resolve, reject) => {
            const request = new Request();
    
            this.client.getAll(request, (err, response: Response) => {
                if (err != null) {
                    reject(err); return;
                }
                const users = <Array<User>>response.getUsersList();
                resolve(users.map((user) => user.toObject()));
            });
        });
    }

    async getUser(id: number) {
        return new Promise((resolve, reject) => {
            const request = new User();
            request.setId(id);
    
            this.client.get(request, (err, response: Response) => {
                if (err != null) {
                    reject(err); return;
                }
                const user = (<User>response.getUser()).toObject();
                resolve(user);
            });
        });
    }

    async me(token: string) {
        return new Promise((resolve, reject) => {
            const request = new Token();
            request.setToken(token);
    
            this.client.me(request, (err, response: Response) => {
                if (err != null) {
                    reject(err); return;
                }
                const user = (<User>response.getUser()).toObject();
                resolve(user);
            });
        });
    }
}
