import { DataSource } from 'apollo-datasource';
import * as grpc from "grpc";
import { AuthServiceClient } from '../stubs/auth_grpc_pb';
import { User, Token } from "../stubs/auth_pb";

export class AuthDataSource extends DataSource {
    client: AuthServiceClient

    constructor() {
        super();
        this.client = new AuthServiceClient('auth-service:80', grpc.credentials.createInsecure());
    }

    async login(email: string, password: string): Promise<string> {
        return new Promise((resolve, reject) => {
            const request = new User();
            request.setEmail(email);
            request.setPassword(password);
    
            this.client.createToken(request, (err, response: Token) => {
                if (err != null) {
                    reject(err); return;
                }
                resolve(response.getToken());
            });
        });
    }

    async validateToken(token: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const request = new Token();
            request.setToken(token);
    
            this.client.validateToken(request, (err, response: Token) => {
                if (err != null) {
                    reject(err); return;
                }
                resolve(response.getValid());
            });
        });
    }
}
