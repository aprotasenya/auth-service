import { IResolvers } from 'graphql-tools';
import { authenticated } from './validators'

const resolvers: IResolvers = {
    Query: {
        users: authenticated((_, __, { dataSources }) => dataSources.UsersDS.getUsers()),

        user: authenticated((_, { id }, { dataSources }) => dataSources.UsersDS.getUser(id)),

        me: authenticated((_, __, { user }) => user), //dataSources.UsersDS.getUser(user.id)),
    },

    Mutation: {
        login: (_, { email, password }, { dataSources }) => dataSources.AuthDS.login(email, password),

        userCreate: authenticated((_, {email, name, password}, { dataSources }) => dataSources.UsersDS.createUser(email, name, password)),

        userUpdate: authenticated((_, {id, email, name, password}, { dataSources }) => dataSources.UsersDS.updateUser(id, email, name, password)),

        userDelete: authenticated((_, {id}, { dataSources }) => dataSources.UsersDS.deleteUser(id)),
    }
}

export default resolvers;
