import { ApolloServer } from 'apollo-server';

import 'graphql-import-node';
import * as typeDefs from './schema/schema.graphql';
import { AuthDataSource, UsersDataSource } from './datasources';
import resolvers from './resolvers';

const users = new UsersDataSource();
const auth = new AuthDataSource()

interface IContext {
    auth: boolean;
    user?: Object
}

const server = new ApolloServer(
    { 
        typeDefs,
        resolvers,
        context: async ({ req }): Promise<IContext> => {
            let context: IContext = {auth: false, user: undefined};
            const token = req.headers.authentication || null;
            try {
                if (token) {
                    context.auth = await auth.validateToken(<string>token);
                }
                if (context.auth) {
                    context.user = await users.me(<string>token) || undefined;
                }
            } catch {}
            return context;
        },
        introspection: true,
        playground: true,
        dataSources: () => ({
            AuthDS: auth,
            UsersDS: users
        }),
        formatError: error => {
            console.error(error);
            return error;
        },
    });

server.listen()
    .then(({ url }) => console.log(`Server ready at ${url}.`))
