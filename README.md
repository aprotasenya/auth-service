Run it
```
docker-compose up
```

Try it


Swagger UI: http://localhost:8082/swagger/

Or use curl:

```
▶ curl -X POST "http://localhost:8080/v1/users" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"name\": \"test\", \"email\": \"test@test.test\", \"password\": \"test\"}"

▶ curl "http://localhost:8080/v1/users"

▶ curl "http://localhost:8080/v1/users/<UUID>"
```